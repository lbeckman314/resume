A self-driven developer who loves helping organizations increase their
impact by crafting software and solutions that work for people. Online
resume available at
[[liambeckman.com/resume](https://liambeckman.com/resume)]{style="color: my-blue"}.

EDUCATION {#education .unnumbered}
=========

::: {style="color: my-grey"}

------------------------------------------------------------------------
:::

**Bachelor of Science; Computer Science** --- Oregon State University
Postbaccalaureate, 3.64 GPA\
[Graduated June 2019 Corvallis, OR]{style="color: my-grey"}

[]{style="color: my-grey-light"}\
**Bachelor of Science; Biology** --- University of Oregon Presidential
Scholar, 3.34 GPA\
[Graduated June 2017 Eugene, OR]{style="color: my-grey"}

EXPERIENCE {#experience .unnumbered}
==========

::: {style="color: my-grey"}

------------------------------------------------------------------------
:::

-   **Software Workflows** --- 2 years working with and releasing
    projects via VCS.
    [[Git](https://git.liambeckman.com/)]{style="color: my-blue"} and
    [[Jenkins](https://liambeckman.com/jenkins/blue/organizations/jenkins/pipelines/)]{style="color: my-blue"}
    servers hosted on a personal single board computer provide
    continuous integration and delivery for my own software projects.

-   **Unix** --- 2 years developing software on GNU+Linux systems
    (currently running self-compiled 5.1.6 kernel).

-   **Object Oriented Design** --- 1+ years developing software with OOD
    principles in Java and JavaScript.

-   **Scripting Languages** --- 1+ years scripting projects and
    workflows with Python and Bash.

PROJECTS {#projects .unnumbered}
========

::: {style="color: my-grey"}

------------------------------------------------------------------------
:::

**[[Voyager
Index\*](https://voyager-index.herokuapp.com)]{style="color: my-blue"}**
--- quality of life application to help world travelers find their next
home.\
[JavaScript, PostgreSQL, Go, Python, HTML, CSS
[[github.com/voyager-index](https://github.com/voyager-index/)]{style="color: my-blue"}]{style="color: my-grey"}

Spearheaded the development of the server and CLI for the Voyager Index
project --- a world map of over 7,000 cities ranked by 23 user-selected
filters. It includes national and international climate, economic, and
safety data.

-   Standardized server interactions with the database (via the
    asynchronous function swimming\_pool()).

-   Hosted entire PostgreSQL database on personal single board computer
    (Raspbian on RPi3 Model B).

-   Integrated and configured webpack and Node.js development workflows.

[]{style="color: my-grey-light"}\
**[[RemoveMyWaste](https://removemywaste.liambeckman.com/)]{style="color: my-blue"}**
--- map application for hazardous waste removal.\
[Java, MariaDB/MySQL, SQLite, JavaScript, HTML, CSS
[[github.com/RemoveMyWaste](https://github.com/RemoveMyWaste/RemoveMyWaste)]{style="color: my-blue"}]{style="color: my-grey"}

Lead development of RemoveMyWaste, an application for the safe disposal
of hazardous household and industrial materials. Users can locate
disposal centers near them and read information on the materials they
wish to dispose of.

-   Created Android and web interfaces. Hosted web and database
    components on personal server.

[]{style="color: my-grey-light"}\
**[[demo\*](https://liambeckman.com/code/demo)]{style="color: my-blue"}**
--- terminal emulator emulator that allows users to try out programs.\
[Node.js, JavaScript, Go
[[github.com/lbeckman314/demo](https://github.com/lbeckman314/demo)]{style="color: my-blue"}]{style="color: my-grey"}

Created a suite of remote applications to allow users to try out
programs and programming languages by accessing a [[simple web
app](https://voyager-index.herokuapp.com)]{style="color: my-blue"},
[[reading
documentation](https://demo.liambeckman.com/docs-demonstration.html)]{style="color: my-blue"},
or even experimenting from the [[command
line](https://github.com/lbeckman314/demo-go.git)]{style="color: my-blue"}.
Processes run in a lightweight Linux sandbox (currently a Debian chroot
secured with
[[Firejail](https://firejail.wordpress.com/)]{style="color: my-blue"}).

[]{style="color: my-grey-light"}\
**[[withfeathers\*](https://withfeathers.liambeckman.com)]{style="color: my-blue"}**
--- poetry web app and shell program. \"'Hope' is the thing with
feathers - \...\"\
[Python, Flask
[[github.com/lbeckman314/withfeathers](https://github.com/lbeckman314/withfeathers)]{style="color: my-blue"}]{style="color: my-grey"}

Developed a web app and CLI that fetches, parses, and selects a random
poem by Emily Dickinson from [[Project
Gutenberg](https://www.gutenberg.org/ebooks/12242)]{style="color: my-blue"}.
A hostable web interface at
[[withfeathers.liambeckman.com](https://withfeathers.liambeckman.com)]{style="color: my-blue"}
makes these poem selections available to anyone with an internet access.

[]{style="color: my-grey-light"}\
\* Interactive demos available at
[[liambeckman.com/code\#terminal](https://liambeckman.com/code#terminal)]{style="color: my-blue"}
[LaTeX source:
[git.io/fhsem](https://git.io/fhsem)]{style="color: my-red"}

COURSES {#courses .unnumbered}
=======

::: {style="color: my-grey"}

------------------------------------------------------------------------
:::

-   **CS 165** --- Accelerated Introduction To Computer Science

-   **CS 225** --- Discrete Structures In Computer Science

-   **CS 261** --- Data Structures

-   **CS 271** --- Computer Architecture And Assembly Languague

-   **CS 290** --- Web Development

-   **CS 325** --- Analysis Of Algorithms

-   **CS 340** --- Introduction To Databases

-   **CS 344** --- Operating Systems I

-   **CS 361** --- Software Engineering I

-   **CS 362** --- Software Engineering II

-   **CS 372** --- Introduction To Computer Networks

-   **CS 373** --- Defense Against The Dark Arts

-   **CS 467** --- Online Capstone Project

-   **CS 475** --- Introduction To Parallel Programming

EXTRAS {#extras .unnumbered}
======

::: {style="color: my-grey"}

------------------------------------------------------------------------
:::

**University of Oregon Honors Biology Lab** --- Eugene, OR\
[Lab Prep Assistant September, 2014 --- June,
2015]{style="color: my-grey"}

Prepared materials and procedures for The Honors Biology Lab curriculum
at the University of Oregon. Relevant responsibilities included making
and curing petri plate solutions, evaluating states and types of
bacterial growth, and studying the processes and mechanisms of
cytological phenotypic expression and function.

[]{style="color: my-grey-light"}\
**\"Research Experiences for Undergraduates\" Internship at University
of Minnesota** --- Minneapolis, MN\
[Student Researcher---Botanical Genetics May --- August,
2014]{style="color: my-grey"}

Conducted research involving botanical DNA isolation, purification,
sequencing, and analysis; examined the effect of personally designed
genetic markers had on a tropical tree's evolution/phylogeny; presented
results and conclusions at the following scientific conferences:

-   The 2015 AAAS Emerging Researchers National Conference in STEM
    hosted in Washington D.C.

-   The 2015 University of Oregon Undergraduate Research Symposium

-   The 2014 University of Minnesota Undergraduate Symposium

[]{style="color: my-grey-light"}\
**Ecological Apprenticeship at H.J. Andrews Experimental Forest** ---
Blue River, OR\
[Student Researcher---Ecology and Restoration June --- August,
2013]{style="color: my-grey"}

Analyzed forest networks and plant response to fire disturbances;
surveyed plant communities in experimental sub-alpine meadows as part of
ongoing research; published study in *Restoration Ecology: The Journal
of the Society for Ecological Restoration* (\"Vegetation Recovery in
Slash-Pile Scars Following Conifer Removal in a Grassland-Restoration
Experiment\", November 2014).
